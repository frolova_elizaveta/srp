package auth_srp;

/**
 * Пользователь с полями логин, соль и верификатор пароля и соответствующими
 * геттерами и сеттерами.
 */
public class User {
    /*Логин*/
    private String login;
    /*Соль*/
    private String salt;
    /*Верификатор пароля*/
    private String verifier;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getVerifier() {
        return verifier;
    }

    public void setVerifier(String verifier) {
        this.verifier = verifier;
    }
}