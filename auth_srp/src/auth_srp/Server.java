package auth_srp;

import java.io.*;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private static FileHelper fileHelper = new FileHelper();

    private static final int portNumber = 4321;

    private static ServerSocket serverSocket;
    private static Socket clientSocket;
    private static PrintWriter out;
    private static BufferedReader in;

    public static void main(String[] args) {
        //инициализация сокета сервера
        try {
            serverSocket = new ServerSocket(portNumber);
            System.out.println("Server socket created");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        try {

            System.out.println("Waiting for clients...");
            //подключение клиента
            clientSocket = serverSocket.accept();
            System.out.println("Client " + clientSocket.getInetAddress() + ":" + clientSocket.getPort() + " connected");
            //инициализация потоков ввода/вывода сокета
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String line;
            //обработка запросов клиента
            while (true) {
                line = in.readLine();
                if (line != null) {
                    switch (line) {
                        case "reg":
                            registration();
                            break;
                        case "auth":
                            authorization();
                            break;
                        default:
                            System.out.println("Wrong client request");
                            break;
                    }
                }
            }
        } catch (IOException e) {
            if (e.getMessage().equals("Connection reset")) {
                try {
                    clientSocket.close();
                    serverSocket.close();
                    System.out.println("Client disconnected, shutting down...");
                    System.exit(0);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            else {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }

    private static void authorization() throws IOException {
        //получение логина от клиента
        String login = in.readLine();
        //получение случайного числа A (SRP) от клиента
        BigInteger A = new BigInteger(in.readLine(), 16);
        //поиск записи пользователя с таким логином в файле
        User user = fileHelper.readUser(login);
        //если пользователь не найден, сообщить об этом клиенту и прервать авторизацию
        if (user == null) {
            out.println("notfound");
            return;
        }
        //генерирование случайного числа b и B (SRP)
        BigInteger b = Generator.generateSmallB();
        BigInteger B = Generator.generateBigB(b, new BigInteger(user.getVerifier(), 16));
        //получение верификатора пароля из записи пользователя
        BigInteger v = new BigInteger(user.getVerifier(), 16);
        //сообщение клиенту о том, что пользователь найден и отправка клиенту соли и B (SRP)
        out.println("ok");
        out.println(user.getSalt());
        out.println(B.toString(16));
        //вычисление значений u, S и K (SRP)
        BigInteger u = Generator.generateHash(A.toString(16), B.toString(16));
        BigInteger S = Generator.generateSserver(A, b, v, u);
        BigInteger K = Generator.generateHash(S.toString(16));
        //вычисление контрольных значений M1 и M2 (SRP)
        BigInteger M1 = Generator.generateM1(user.getLogin(), user.getSalt(), A, B, K);
        BigInteger M2 = Generator.generateM2(A, M1, K);
        String line;
        //ожидание контрольного числа M1 от клиента
        while ((line = in.readLine()) == null);
        //если сообщение отличается от требуемого - ошибка
        if (!line.equals("check")) {
            System.out.println("Wrong client request");
            return;
        }
        //получение контрольного числа M1 от клиента
        BigInteger M1_client = new BigInteger(in.readLine(), 16);
        /* сравнение контрольного числа M1 от клиента с собственным
        если не совпадают - ошибка, сообщить об этом клиенту, прервать авторизацию
         */
        if (M1.compareTo(M1_client) != 0) {
            out.println("fail");
            System.out.println("M1 do not match!");
            return;
        }
        //если совпадают - сообщить клиенту и отправить ему контрольное число M2 для проверки
        out.println("ok");
        out.println(M2.toString(16));
        //ожидание результата проверки M2 от клиента
        while ((line = in.readLine()) == null);
        //обработка сообщения клиента о результатах проверки M2
        if (line.equals("allok"))
            System.out.println("Authorization succeed");
        else if (line.equals("fail"))
            System.out.println("Something's wrong with client's M2");
        else
            System.out.println("Wrong client response");

    }

    private static void registration() throws IOException {
        //получение логина, соли и верификатора от клиента
        String login = in.readLine();
        String salt = in.readLine();
        String verifier = in.readLine();
        /*если такого пользователя еще нет - сделать новую запись, и сообщить клиенту об успехе,
        иначе - сообщить клиенту об ошибке */
        if (fileHelper.readUser(login) == null) {
            fileHelper.writeUser(login, salt, verifier);
            out.println("ok");
        }
        else
            out.println("error");

    }
}
