package auth_srp;

import java.io.*;
import java.math.BigInteger;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private static final String hostName = "localhost";
    private static final int portNumber = 4321;

    private static Socket clientSocket;
    private static PrintWriter out;
    private static BufferedReader in;
    private static Scanner user_in;

    public static void main(String[] args) {
        try {
            //создание сокета клиента и подклюение к серверу
            clientSocket = new Socket(hostName, portNumber);
            //инициализация потоков ввода/вывода сокета
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            //инициализация консольного ввода клиента
            user_in = new Scanner(System.in);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        while (true) {
            //вывод информации о командах
            System.out.println("0 - Exit\n1 - Registration\n2 - Authorization");
            //получение ответа пользователя
            int userAnswer = user_in.nextInt();
            user_in.nextLine();
            //обработка ответа пользователя
            if (userAnswer == 0)
                System.exit(0);
            else if (userAnswer == 1)
                registration();
            else if (userAnswer == 2)
                authorization();
            else
                System.out.println("Bad input");
        }
    }

    private static void registration() {
        //считывание логин, пароль и подтверждение пароля
        System.out.println("Enter login, please: ");
        String userLogin = user_in.nextLine();
        System.out.println("Enter password, please: ");
        String userPassword = user_in.nextLine();
        System.out.println("Repeat password, please: ");
        /*проверка, совпадают ли пароль и подтверждение пароля
        если нет - вывести сообщение, прервать регистрацию */
        if (!userPassword.equals(user_in.nextLine())) {
            System.out.println("Ooops! Passwords do not match!");
            return;
        }
        //генерирование соль, x и верификатор пароля (SRP)
        String salt = Generator.generateSalt();
        BigInteger x = Generator.generateHash(salt, userPassword);
        BigInteger v = Generator.generateV(x);
        //отправка серверу запроса на регистрацию и данных для регистрации
        out.println("reg");
        out.println(userLogin);
        out.println(salt);
        out.println(v.toString(16));
        String line;
        try {
            //ожидание ответа сервера
            while ((line = in.readLine()) == null);
            //вывод ответа сервера
            if (line.equals("ok"))
                System.out.println("Registration succesful!");
            else if (line.equals("error"))
                System.out.println("Such login is already used");
            else
                System.out.println("Wrong server response");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void authorization() {
        //считывание логина и пароля
        System.out.println("Enter login, please: ");
        String userLogin = user_in.nextLine();
        System.out.println("Enter password, please: ");
        String userPassword = user_in.nextLine();
        //генерирование случайного числа a и A (SRP)
        BigInteger a = Generator.generateSmallA();
        BigInteger A = Generator.generateBigA(a);
        //отправка серверу запроса на авторизации и данных для авторизации
        out.println("auth");
        out.println(userLogin);
        out.println(A.toString(16));
        String line;
        String salt;
        BigInteger B;
        try {
            //ожидание ответа сервера
            while ((line = in.readLine()) == null);
            //если сервер сообщил, что такого пользователя нет - вывести ошибку, прервать регистрацию
            if (line.equals("notfound")) {
                System.out.println("No such user!");
                return;
            }
            else if (!line.equals("ok")) {
                System.out.println("Wrong server response");
                return;
            }
            //получить соль и число B (SRP) от сервера
            salt = in.readLine();
            B = new BigInteger(in.readLine(), 16);
            //вычисление значений u, х, S и K (SRP)
            BigInteger u = Generator.generateHash(A.toString(16), B.toString(16));
            BigInteger x = Generator.generateHash(salt, userPassword);
            BigInteger S = Generator.generateSclient(B, a, x, u);
            BigInteger K = Generator.generateHash(S.toString(16));
            //вычисление контрольных значений M1 и M2 (SRP)
            BigInteger M1 = Generator.generateM1(userLogin, salt, A, B, K);
            BigInteger M2 = Generator.generateM2(A, M1, K);
            //запрос серверу на проверку M1
            out.println("check");
            out.println(M1.toString(16));
            //ожидание ответа от сервера
            while ((line = in.readLine()) == null);
            //если проверка не удалась, вывести сообщение и прервать авторизацию
            if (line.equals("fail")) {
                System.out.println("Sorry, login or password incorrect.");
                return;
            }
            else if (!line.equals("ok")) {
                System.out.println("Wrong server response");
                return;
            }
            //получить контрольное число M2 от сервера
            BigInteger M2_server = new BigInteger(in.readLine(), 16);
            //проверка M2. если проверка не удалась, вывести сообщение и прервать авторизацию
            if (M2.compareTo(M2_server) != 0) {
                out.println("fail");
                System.out.println("Sorry, login or password incorrect.");
                return;
            }
            //если проверка прошла успешно, вывести сообщение и сообщить серверу
            System.out.println("Congratulations! Authorization succeed");
            out.println("allok");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
