package auth_srp;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

public class Generator {


    public static final BigInteger N = new BigInteger("EEAF0AB9ADB38DD69C33F80AFA8FC5E86072618775FF3C0BEA2314C" +
            "9C256576D674DF7496EA81D3383B4813D692C6E0E0D5D8E250B98BE4" +
            "8E495C1D6089DAD15DC7D7B46154D6B6CE8EF4AD69B15D4982559B29" +
            "7BCF1885C529F566660E57EC68EDBC3C05726CC02FD4CBF4976EAA9A" +
            "FD5138FE8376435B9FC61D2FC0EB06E3", 16);
    public static final BigInteger g = BigInteger.valueOf(2);
    public static final BigInteger k = BigInteger.valueOf(3);
    public static final int bit_length = 1024;

    /**
     * Генерация соли. Соль возвращается в виде строки
     */
    public static String generateSalt() {
        final Random r = new SecureRandom();
        byte[] salt = new byte[32];
        r.nextBytes(salt);
        return String.format("%064x", new java.math.BigInteger(1, salt));
    }

    /**
     * Вычисление хэша. Входные параметры - несколько строк;
     * хэш возвращается в виде строки.
     */
    public static BigInteger generateHash(String... params) {
        String text = "";
        for (String str : params) {
            text = text + str;
        }
        /*Вычисление хэш-функции от пароля и соли*/
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
            try {
                md.update(text.getBytes("UTF-8")); // Change this to "UTF-16" if needed
            } catch (UnsupportedEncodingException uee) {
                uee.printStackTrace();
            }
            byte[] digest = md.digest();
            /*Перевод в шестнадцатиричную систему*/
            String result = String.format("%064x", new java.math.BigInteger(1, digest));
            return new BigInteger(result, 16);

        } catch (NoSuchAlgorithmException nsae) {
            nsae.printStackTrace();
        }
        return null;
    }
    /**
     * Функции для арифметики SRP
     */

    public static BigInteger generateV(BigInteger x) {
        BigInteger v = g.modPow(x, N);
        return v;
    }

    public static BigInteger generateSmallA() {
        BigInteger a = (new BigInteger(bit_length, new Random())).mod(N);
        return a;
    }

    public static BigInteger generateSmallB() {
        BigInteger b = (new BigInteger(bit_length, new Random())).mod(N);
        return b;
    }

    public static BigInteger generateBigA(BigInteger a) {
        return g.modPow(a, N);
    }

    public static BigInteger generateBigB(BigInteger b, BigInteger v) {
        BigInteger kv = k.multiply(v).mod(N);
        return g.modPow(b, N).add(kv).mod(N);

    }

    public static BigInteger generateSclient(BigInteger B, BigInteger a, BigInteger x, BigInteger u) {
        BigInteger t1 = u.multiply(x).mod(N).add(a).mod(N);
        BigInteger t2 = g.modPow(x, N).multiply(k).mod(N);
        BigInteger t3 = B.subtract(t2).mod(N);
        return t3.modPow(t1, N);
    }

    public static BigInteger generateSserver(BigInteger A, BigInteger b, BigInteger v, BigInteger u) {
        return v.modPow(u, N).multiply(A).mod(N).modPow(b, N);
    }

    public static BigInteger generateM1(String login, String salt, BigInteger A, BigInteger B, BigInteger K) {
        String t1 = (generateHash(N.toString(16)).xor(generateHash(g.toString(16)))).toString(16);
        return generateHash(t1, generateHash(login).toString(16), salt, A.toString(16), B.toString(16), K.toString(16));
    }

    public static BigInteger generateM2(BigInteger A, BigInteger M1, BigInteger K) {
        return generateHash(A.toString(16), M1.toString(16), K.toString(16));
    }

}
