package auth_srp;

import java.io.*;

/**
 * Класс, позволяющий записать/считать данные о пользователях в/из txt-файл(а).
 * При создании объекта данного класса, на основе имеющегося txt-файла формируется
 * список пользователей с информацией о них - users.
 */
public class FileHelper {
    /*Название txt-файла*/
    private static final String FILENAME = "users.txt";
    private FileWriter myFileWriter;
    private BufferedWriter myBufferedWriter;
    private FileReader myFileReader;
    private BufferedReader myBufferedReader;

    public void writeUser(String login, String salt, String verifier) {
        try {
            /*Запись будет производиться в файмл с указанным именем.
            * Второй параметр указывает на то, что данные будет записываться
            * без удаления предыдущих записей в файле.*/
            myFileWriter = new FileWriter(FILENAME, true);
            myBufferedWriter = new BufferedWriter(myFileWriter);
            /*Записать последовательно логин, соль и верификатор*/
            myBufferedWriter.write(login + "\n");
            myBufferedWriter.write(salt + "\n");
            myBufferedWriter.write(verifier + "\n");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                myBufferedWriter.flush();
                myBufferedWriter.close();
                myFileWriter.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public User readUser(String login) {
        try {
            /*Чтение будет производиться из файла с указанным именем*/
            myFileReader = new FileReader(FILENAME);
            myBufferedReader = new BufferedReader(myFileReader);
            //создание нового пользователя
            User user = new User();
            String line;
            //флаг о нахождении пользователя с указанным логином
            boolean found = false;
            while ((line = myBufferedReader.readLine()) != null)
                //если нашли пользователя с указанным логином
                if (line.equals(login)) {
                    //поднять флаг о нахождении пользователя
                    found = true;
                    //считать данные пользователя из файла в объект класса User
                    user.setLogin(login);
                    user.setSalt(myBufferedReader.readLine());
                    user.setVerifier(myBufferedReader.readLine());
                    break;
                }
            //если пользователь был найден, вернуть объект класса User с данными
            if (found)
                return user;
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                myBufferedReader.close();
                myFileReader.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        //если пользователь не был найден, вернуть null
        return null;
    }

}
